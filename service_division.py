import asyncio
from nats.aio.client import Client as NATS
from nats.aio.errors import ErrConnectionClosed, ErrTimeout, ErrNoServers
import json

async def run(loop):
    nc = NATS()

    await nc.connect("demo.nats.io:4222", loop=loop)

    async def division_list(msg):
        subject = msg.subject
        reply = msg.reply
        data = msg.data.decode()
        print("Received a message on '{subject} {reply}': {data}".format(
            subject=subject, reply=reply, data=data))
        await nc.publish(reply, json.dumps({"divs": [{"id_div": 0,"name": ""}],"offset": 0,"limit": 0,"total": 0,"error": "","status": ""}).encode())

    # Use queue named 'workers' for distributing requests
    # among subscribers.
    await nc.subscribe("division_list", "workers", division_list)

    async def division_add(msg):
        subject = msg.subject
        reply = msg.reply
        data = msg.data.decode()
        print("Received a message on '{subject} {reply}': {data}".format(
            subject=subject, reply=reply, data=data))
        await nc.publish(reply, json.dumps({"error": "","status": ""}).encode())

    # Use queue named 'workers' for distributing requests
    # among subscribers.
    await nc.subscribe("division_add", "workers", division_add)

    async def division_edit(msg):
        subject = msg.subject
        reply = msg.reply
        data = msg.data.decode()
        print("Received a message on '{subject} {reply}': {data}".format(
            subject=subject, reply=reply, data=data))
        await nc.publish(reply, json.dumps({"error": "","status": ""}).encode())

    # Use queue named 'workers' for distributing requests
    # among subscribers.
    await nc.subscribe("division_edit", "workers", division_edit)

    async def division_delete(msg):
        subject = msg.subject
        reply = msg.reply
        data = msg.data.decode()
        print("Received a message on '{subject} {reply}': {data}".format(
            subject=subject, reply=reply, data=data))
        await nc.publish(reply, json.dumps({"error": "","status": ""}).encode())

    # Use queue named 'workers' for distributing requests
    # among subscribers.
    await nc.subscribe("division_delete", "workers", division_delete)

    print("Listening for requests on 'division' subject...")

if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    loop.run_until_complete(run(loop))
    loop.run_forever()
    loop.close()