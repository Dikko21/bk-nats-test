import asyncio
from nats.aio.client import Client as NATS
from nats.aio.errors import ErrConnectionClosed, ErrTimeout, ErrNoServers
import json

async def run(loop):
    nc = NATS()

    await nc.connect("demo.nats.io:4222", loop=loop)

    async def user_login(msg):
        subject = msg.subject
        reply = msg.reply
        data = msg.data.decode()
        print("Received a message on '{subject} {reply}': {data}".format(
            subject=subject, reply=reply, data=data))
        await nc.publish(reply, json.dumps({"error": "","status": "","id_token": ""}).encode())

    # Use queue named 'workers' for distributing requests
    # among subscribers.
    await nc.subscribe("user_login", "workers", user_login)

    async def user_reset(msg):
        subject = msg.subject
        reply = msg.reply
        data = msg.data.decode()
        print("Received a message on '{subject} {reply}': {data}".format(
            subject=subject, reply=reply, data=data))
        await nc.publish(reply, json.dumps({"error": "","status": ""}).encode())

    # Use queue named 'workers' for distributing requests
    # among subscribers.
    await nc.subscribe("user_reset", "workers", user_reset)

    async def user_changepass(msg):
        subject = msg.subject
        reply = msg.reply
        data = msg.data.decode()
        print("Received a message on '{subject} {reply}': {data}".format(
            subject=subject, reply=reply, data=data))
        await nc.publish(reply, json.dumps({"error": "","status": ""}).encode())

    # Use queue named 'workers' for distributing requests
    # among subscribers.
    await nc.subscribe("user_changepass", "workers", user_changepass)

    print("Listening for requests on 'user' subject...")

if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    loop.run_until_complete(run(loop))
    loop.run_forever()
    loop.close()