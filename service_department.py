import asyncio
from nats.aio.client import Client as NATS
from nats.aio.errors import ErrConnectionClosed, ErrTimeout, ErrNoServers
import json

async def run(loop):
    nc = NATS()

    await nc.connect("demo.nats.io:4222", loop=loop)

    async def department_list(msg):
        subject = msg.subject
        reply = msg.reply
        data = msg.data.decode()
        print("Received a message on '{subject} {reply}': {data}".format(
            subject=subject, reply=reply, data=data))
        await nc.publish(reply, json.dumps({"depts": [{"id_dept": 0,"name": ""}],"offset": 0,"limit": 0,"total": 0,"error": "","status": ""}).encode())

    # Use queue named 'workers' for distributing requests
    # among subscribers.
    await nc.subscribe("department_list", "workers", department_list)

    async def department_add(msg):
        subject = msg.subject
        reply = msg.reply
        data = msg.data.decode()
        print("Received a message on '{subject} {reply}': {data}".format(
            subject=subject, reply=reply, data=data))
        await nc.publish(reply, json.dumps({"error": "","status": ""}).encode())

    # Use queue named 'workers' for distributing requests
    # among subscribers.
    await nc.subscribe("department_add", "workers", department_add)

    async def department_edit(msg):
        subject = msg.subject
        reply = msg.reply
        data = msg.data.decode()
        print("Received a message on '{subject} {reply}': {data}".format(
            subject=subject, reply=reply, data=data))
        await nc.publish(reply, json.dumps({"error": "","status": ""}).encode())

    # Use queue named 'workers' for distributing requests
    # among subscribers.
    await nc.subscribe("department_edit", "workers", department_edit)

    async def department_delete(msg):
        subject = msg.subject
        reply = msg.reply
        data = msg.data.decode()
        print("Received a message on '{subject} {reply}': {data}".format(
            subject=subject, reply=reply, data=data))
        await nc.publish(reply, json.dumps({"error": "","status": ""}).encode())

    # Use queue named 'workers' for distributing requests
    # among subscribers.
    await nc.subscribe("department_delete", "workers", department_delete)

    print("Listening for requests on 'department' subject...")

if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    loop.run_until_complete(run(loop))
    loop.run_forever()
    loop.close()