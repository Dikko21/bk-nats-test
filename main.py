from sanic import Sanic
from sanic.response import json
import asyncio
from nats.aio.client import Client as NATS
from nats.aio.errors import ErrConnectionClosed, ErrTimeout, ErrNoServers
import json as json2

app = Sanic("App Name")
status = "dev"

@app.post(f"/app/plm/{status}/user/login")
async def userLogin(request):
    body = request.json
    result = ""
    nc = NATS()
    await nc.connect("demo.nats.io:4222")

    try:
        response = await nc.request("user_login", b'user_login_message', timeout=2)
        result = json2.loads(response.data.decode())
    except ErrTimeout:
        print("Request timed out")

    await nc.close()

    return json(result)

@app.post(f"/app/plm/{status}/user/reset")
async def userReset(request):
    body = request.json
    result = ""
    nc = NATS()
    await nc.connect("demo.nats.io:4222")

    try:
        response = await nc.request("user_reset", b'user_reset_message', timeout=2)
        result = json2.loads(response.data.decode())
    except ErrTimeout:
        print("Request timed out")

    await nc.close()

    return json(result)

@app.put(f"/app/plm/{status}/user/changepass")
async def userChangepass(request):
    body = request.json
    result = ""
    nc = NATS()
    await nc.connect("demo.nats.io:4222")

    try:
        response = await nc.request("user_changepass", b'user_changepass_message', timeout=2)
        result = json2.loads(response.data.decode())
    except ErrTimeout:
        print("Request timed out")

    await nc.close()

    return json(result)

@app.post(f"/app/plm/{status}/department/list")
async def userChangepass(request):
    body = request.json
    result = ""
    nc = NATS()
    await nc.connect("demo.nats.io:4222")

    try:
        response = await nc.request("department_list", b'department_list_message', timeout=2)
        result = json2.loads(response.data.decode())
    except ErrTimeout:
        print("Request timed out")

    await nc.close()

    return json(result)

@app.post(f"/app/plm/{status}/department/add")
async def userChangepass(request):
    body = request.json
    result = ""
    nc = NATS()
    await nc.connect("demo.nats.io:4222")

    try:
        response = await nc.request("department_add", b'department_add_message', timeout=2)
        result = json2.loads(response.data.decode())
    except ErrTimeout:
        print("Request timed out")

    await nc.close()

    return json(result)

@app.put(f"/app/plm/{status}/department/edit")
async def userChangepass(request):
    body = request.json
    result = ""
    nc = NATS()
    await nc.connect("demo.nats.io:4222")

    try:
        response = await nc.request("department_edit", b'department_edit_message', timeout=2)
        result = json2.loads(response.data.decode())
    except ErrTimeout:
        print("Request timed out")

    await nc.close()

    return json(result)

@app.delete(f"/app/plm/{status}/department/delete")
async def userChangepass(request):
    body = request.json
    result = ""
    nc = NATS()
    await nc.connect("demo.nats.io:4222")

    try:
        response = await nc.request("department_delete", b'department_delete_message', timeout=2)
        result = json2.loads(response.data.decode())
    except ErrTimeout:
        print("Request timed out")

    await nc.close()

    return json(result)

@app.post(f"/app/plm/{status}/division/list")
async def userChangepass(request):
    body = request.json
    result = ""
    nc = NATS()
    await nc.connect("demo.nats.io:4222")

    try:
        response = await nc.request("division_list", b'division_list_message', timeout=2)
        result = json2.loads(response.data.decode())
    except ErrTimeout:
        print("Request timed out")

    await nc.close()

    return json(result)

@app.post(f"/app/plm/{status}/division/add")
async def userChangepass(request):
    body = request.json
    result = ""
    nc = NATS()
    await nc.connect("demo.nats.io:4222")

    try:
        response = await nc.request("division_add", b'division_add_message', timeout=2)
        result = json2.loads(response.data.decode())
    except ErrTimeout:
        print("Request timed out")

    await nc.close()

    return json(result)

@app.put(f"/app/plm/{status}/division/edit")
async def userChangepass(request):
    body = request.json
    result = ""
    nc = NATS()
    await nc.connect("demo.nats.io:4222")

    try:
        response = await nc.request("division_edit", b'division_edit_message', timeout=2)
        result = json2.loads(response.data.decode())
    except ErrTimeout:
        print("Request timed out")

    await nc.close()

    return json(result)

@app.delete(f"/app/plm/{status}/division/delete")
async def userChangepass(request):
    body = request.json
    result = ""
    nc = NATS()
    await nc.connect("demo.nats.io:4222")

    try:
        response = await nc.request("division_delete", b'division_delete_message', timeout=2)
        result = json2.loads(response.data.decode())
    except ErrTimeout:
        print("Request timed out")

    await nc.close()

    return json(result)

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=8000)